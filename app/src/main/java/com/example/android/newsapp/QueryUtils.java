package com.example.android.newsapp;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


public class QueryUtils {
    private static final String LOG_TAG = QueryUtils.class.getName();
    private static Context context;

    public QueryUtils(Context context) {
        this.context = context;
    }

    public static List<Article> fetchData(String stringUrl) {
        URL url = createURL(stringUrl);
        String jsonResponse = "";
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "HTTP Request error " + e.getMessage());
        }

        List<Article> articleList = extractArticleData(jsonResponse);
        return articleList;
    }

    private static URL createURL(String stringURL) {
        URL url = null;
        try {
            url = new URL(stringURL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "Create url error: ", e.getCause());
        }
        return url;
    }

    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";
        if (url == null) {
            return jsonResponse;
        }

        HttpsURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readInputStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Response CODE is --> " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readInputStream(InputStream input) throws IOException {
        StringBuilder output = new StringBuilder();
        if (input != null) {
            InputStreamReader inputReader = new InputStreamReader(input, Charset.forName("UTF-8"));
            BufferedReader buffer = new BufferedReader(inputReader);
            String line = buffer.readLine();
            while (line != null) {
                output.append(line);
                line = buffer.readLine();
            }
        }
        return output.toString();
    }

    public static List<Article> extractArticleData(String jsonResponse) {
        if (TextUtils.isEmpty(jsonResponse)) {
            return null;
        }
        List<Article> articles = new ArrayList<>();
        try {
            JSONObject root = new JSONObject(jsonResponse);
            JSONObject response = root.getJSONObject("response");
            JSONArray results = new JSONArray();
            if (response.has("editorsPicks")) {
                results = response.getJSONArray("editorsPicks");
            } else if (response.has("results")) {
                results = response.getJSONArray("results");
            }

            for (int i = 0; i < results.length(); i++) {
                JSONObject article = results.getJSONObject(i);
                String title = article.getString("webTitle");
                String url = article.getString("webUrl");

                String section = "";
                if (article.has("sectionName")) {
                    section = article.getString("sectionName");
                }

                String date = article.getString("webPublicationDate");

                JSONObject fields = article.getJSONObject("fields");
                String thumbnail = "";
                if (fields.has("thumbnail")) {
                    thumbnail = fields.getString("thumbnail");
                }

                String author = "";
                if (fields.has("byline")) {
                    author = fields.getString("byline");
                }
                String content = fields.getString("bodyText");
                Article item = new Article(title, date, author, content, section, url, thumbnail);
                articles.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return articles;
    }
}
