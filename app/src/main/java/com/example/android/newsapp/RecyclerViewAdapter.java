package com.example.android.newsapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    List<Article> articlesList;
    LayoutInflater inflater;
    Context context;
    private static final String LOG_TAG = RecyclerViewAdapter.class.getName();

    public RecyclerViewAdapter(Context context, List<Article> articlesList) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.articlesList = articlesList;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final View view = inflater.inflate(R.layout.article_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Article currentArticle = articlesList.get(position);
        holder.title.setText(currentArticle.getTitle());
        holder.author.setText("by " + currentArticle.getAuthor());

        String currentDate = currentArticle.getDate();
        String newDate = formatDate(currentDate);
        holder.date.setText(newDate);

        holder.section.setText("Section: " + currentArticle.getSection());
        holder.content.setText(currentArticle.getContent());

        //get thumbnail
        String imageUri = currentArticle.getThumbnail();
        Uri uri = Uri.parse(imageUri);
        if (imageUri != null && !imageUri.isEmpty()) {
            Picasso.with(context).load(uri).into(holder.thumb);
        } else {
            holder.thumb.setImageResource(R.drawable.news_thumb);
        }

        //go to link on item click
        holder.article_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(currentArticle.getLink()));
                context.startActivity(intent);
            }
        });
    }

    public void addAll(List<Article> data) {
        articlesList.clear();
        articlesList.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        int items = this.articlesList.size();
        return items;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.author)
        TextView author;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.imageView)
        ImageView thumb;
        @BindView(R.id.section)
        TextView section;
        @BindView(R.id.content)
        TextView content;
        @BindView(R.id.article_layout)
        View article_layout;


        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private String formatDate(String currentDate) {
        String newDate = "";
        try {
            //transform current date format into Date object
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.ENGLISH);
            Date date = format.parse(currentDate);

            //change format -> we do this because the format() only takes a Date object
            format = new SimpleDateFormat("EEEE, dd MMMM yyyy hh:mm a", Locale.ENGLISH);
            newDate = format.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(LOG_TAG, "Error formatting Date: " + e.getMessage());
        }
        return newDate;
    }
}
