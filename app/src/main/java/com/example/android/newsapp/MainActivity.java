package com.example.android.newsapp;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Article>> {

    ArrayAdapter listAdapter;
    StaggeredGridLayoutManager layoutManager;
    private static final String LOG_TAG = MainActivity.class.getName();

    private static String today = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH).format(Calendar.getInstance().getTime());
    private static final int LOADER_ID = 1;
    private String query = "";
    private static String searchQuery = "";
    public boolean searching = false;
    private String clientQuery = "";

    RecyclerViewAdapter adapter;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.empty_tv)
    TextView empty_tv;


    private static String HEADLINES_UK = "https://content.guardianapis.com/uk?show-editors-picks=true&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static String US_HEADLINES = "https://content.guardianapis.com/us?show-editors-picks=true&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static String WORLD_HEADLINES = "https://content.guardianapis.com/world?show-editors-picks=true&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static String SEARCH_URL = "https://content.guardianapis.com/search?show-fields=all&order-by=newest&page-size=100&api-key=test&q=";
    private static final String TRAVEL = "https://content.guardianapis.com/travel?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static final String BUSINESS = "https://content.guardianapis.com/business?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static final String CULTURE = "https://content.guardianapis.com/culture?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static final String SPORT = "https://content.guardianapis.com/sport?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static final String TECHNOLOGY = "https://content.guardianapis.com/technology?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static final String SCIENCE = "https://content.guardianapis.com/science?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static final String FASHION = "https://content.guardianapis.com/fashion?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static final String ENVIRONMENT = "https://content.guardianapis.com/environment?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static final String MONEY = "https://content.guardianapis.com/money?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static final String OBSERVER = "https://content.guardianapis.com/theobserver?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static final String OPINION = "https://content.guardianapis.com/commentisfree?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static final String LIFESTYLE = "https://content.guardianapis.com/lifeandstyle?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";
    private static final String ROMANIA = "https://content.guardianapis.com/world/romania?show-elements=image&format=json&show-fields=all&order-by=newest&page-size=100&api-key=test";

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mPlanetTitles;
    private static boolean isLaunch = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //set layout
        recyclerView.setHasFixedSize(true);
        int screenWidth = getResources().getConfiguration().screenWidthDp;
        if (screenWidth <= 360) {//j5 portrait
            layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        } else if (screenWidth > 360 && screenWidth <= 600) {//asus portrait
            layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        } else if (screenWidth > 600 && screenWidth <= 800) {//j5 landscape
            layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        } else if (screenWidth > 800) {//asus landscape
            layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        }

        recyclerView.setLayoutManager(layoutManager);
        RecyclerView.ItemAnimator animator = new DefaultItemAnimator();
        animator.setAddDuration(1000);
        animator.setRemoveDuration(1000);
        recyclerView.setItemAnimator(animator);

        //set adapter
        adapter = new RecyclerViewAdapter(this, new ArrayList<Article>());
        recyclerView.setAdapter(adapter);

        //check network connectivity
        if (isOnline()) {
            query = WORLD_HEADLINES;
            getLoaderManager().initLoader(LOADER_ID, null, this).forceLoad();
        } else {
            progressBar.setVisibility(View.GONE);
            empty_tv.setVisibility(View.VISIBLE);
            empty_tv.setText("Network not available");
        }

        //drawer navigation
        mTitle = mDrawerTitle = getTitle();
        mPlanetTitles = getResources().getStringArray(R.array.menu_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        listAdapter = new ArrayAdapter<String>(this, R.layout.drawer_list_item, mPlanetTitles);
        mDrawerList.setAdapter(listAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                //R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            @Override
            public void onDrawerClosed(View view) {
                if (clientQuery != null && !clientQuery.isEmpty()) {
                    String queryTitle = clientQuery.replace("+", " ");
                    mTitle = queryTitle;
                }
                getSupportActionBar().setTitle(mTitle);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerStateChanged(int newState) {
                super.onDrawerStateChanged(newState);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerLayout.addDrawerListener(mDrawerToggle);
        if (isLaunch) {
            isLaunch = false;
            selectItem(0);
        }

        //set searchview in the drawer header
        ViewGroup searchGroup = (ViewGroup) getLayoutInflater().inflate(R.layout.nav_header, mDrawerList, false);
        mDrawerList.addHeaderView(searchGroup, null, false);

        //setup search view
        //I can't use bindview with the searchView here -> java.lang.IllegalStateException
        final SearchView searchView = (SearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //perform search only if there is network connectivity
                if (isOnline()) {
                    searching = true;
                    progressBar.setVisibility(View.VISIBLE);
                    clientQuery = searchView.getQuery().toString();
                    clientQuery = clientQuery.replace(" ", "+");
                    searchQuery = SEARCH_URL + clientQuery.trim();

                    Log.v(LOG_TAG, clientQuery);
                    getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                    searchView.clearFocus();
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    progressBar.setVisibility(View.GONE);
                    empty_tv.setVisibility(View.VISIBLE);
                    empty_tv.setText("Network not available");
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    //NETWORK secton
    private boolean isOnline() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectivityManager.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    //DRAWER section
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /* The click listener for ListView in the navigation drawer */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            position -= mDrawerList.getHeaderViewsCount();
            clientQuery = null;
            selectItem(position);
            mDrawerList.setItemChecked(position+1, true);
        }
    }

    private void selectItem(int position) {
        // update the main content by replacing fragments
        // Handle Navigation Options
        switch (position) {
            case 0:
                query = WORLD_HEADLINES;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 1:
                query = HEADLINES_UK;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 2:
                query = US_HEADLINES;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 3:
                query = BUSINESS;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 4:
                query = ENVIRONMENT;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 5:
                query = MONEY;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 6:
                query = TECHNOLOGY;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 7:
                query = SCIENCE;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 8:
                query = TRAVEL;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 9:
                query = CULTURE;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 10:
                query = LIFESTYLE;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 11:
                query = FASHION;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 12:
                query = SPORT;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 13:
                query = OBSERVER;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 14:
                query = OPINION;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            case 15:
                query = ROMANIA;
                getLoaderManager().restartLoader(LOADER_ID, null, MainActivity.this);
                break;
            default:
                break;
        }
        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mPlanetTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    //LOADER section

    @Override
    public Loader<List<Article>> onCreateLoader(int id, Bundle args) {
        if (searching) {
            searching = false;
            return new ArticleLoader(this, searchQuery);
        }
        return new ArticleLoader(this, query);
    }

    @Override
    public void onLoadFinished(Loader<List<Article>> loader, List<Article> data) {
        progressBar.setVisibility(View.GONE);
        if (data != null && !data.isEmpty()) {
            Log.v(LOG_TAG, "LOADING DATA IN ADAPTER");
            adapter.addAll(data);
            empty_tv.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.GONE);
            empty_tv.setVisibility(View.VISIBLE);
            empty_tv.setText("Your search returned no results. \nDo you want to try again?");
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Article>> loader) {
        recyclerView.getRecycledViewPool().clear();
    }
}
