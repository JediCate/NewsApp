package com.example.android.newsapp;

public class Article {
    private String title;
    private String date;
    private String author;
    private String content;
    private String section;
    private String link;
    private String thumbnail;

    public Article(){ }

    public Article(String title, String date, String author, String content, String section, String link, String thumbnail){
        this.title = title;
        this.date = date;
        this.author = author;
        this.content = content;
        this.section= section;
        this.link = link;
        this.thumbnail = thumbnail;
    }

    public String getTitle(){return title;}
    public String getDate(){return date;}
    public String getAuthor(){return author;}
    public String getContent(){return content;}
    public String getSection(){return section;}
    public String getLink(){return link;}
    public String getThumbnail(){return thumbnail;}

    @Override
    public String toString() {
        return "Article title: " + title + "\nAuthor: " + author + "\nSection: " + section + "\nContent: " + content + "\nLink: " + link + "\nThumbnail" + thumbnail;
    }
}
